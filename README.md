Тестовое задание для Frontend разработчика.
----
Описание задачи
----
Реализовать страницу для работы со списком курсов: Добавление, редактирование, просмотр, удаление. 

Каждый курс должен иметь следующий набор полей:

 - id
 - Название
 - Описание
 - Цена
 - Дата начала

Необходимый функционал: 

Основные требования
 - Существовать страница всех курсов
 - Существовать страница курса где будет возможность просмотра\удаления\редактирования выбранного курса
 - Возможность добавления нового курса в список
 - Сортировка списка по цене или дате

Дополнительные требования
 - Реализовать компонент пагинации и добавление его на страницу всех курсов. Компонент должен быть переиспользуем под любые данные
 - Реализовать валидацию форм на все операции создание\удаление\редактирование и их вывод в интерфейсе

Обязательные требования к решению:
----
 - Разработать все пункты из основных требований
 - Пункты из дополнительных требований не обязательны, но будут вам в плюс
 - Nuxt.js последней стабильной версии
 - Все курсы должны быть доступны по ссылке (прим. /courses/course/:id)
 - Сделать мок данных data.json из 6 (или более) курсов с заполненными данными
 - При первой загрузке, перед рендерингом страницы - записать данные из data.json во Vuex и в дальнейшем работать через Vuex
 
Дизайн не имеет значения

Все компоненты, сортировка, пагинация, и прочие, должны быть написаны самостоятельно! Без использования готовых библиотек\плагинов и т.д.
